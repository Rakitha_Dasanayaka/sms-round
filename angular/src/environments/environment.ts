// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
	production: false,
	isMockEnabled: true, // You have to switch this, when your real back-end is done
	authTokenKey: 'authce9d77b308c149d5992a80073637e4d5',
	//baseURL: 'http://34.70.53.145',
	baseURL: 'https://smsround.vision8.io',
	apiKey: '0254eada-485d-14d9-b5561-d62673d93L',

	adminLoginAPI: '/api/auth/user/login',

	// adminLoginAPI: '/api/auth/admin/loginAdmin',

	getUserDetailsAPI: '/api/auth/user/myAccount',
	testAPI: '/api/app/test_version',
	getContactusAPI: '/api/contacts/contactus/list',
	getJobApplicationAPI: '/api/contacts/jobApplication/list',
	getNewsletterAPI: '/api/contacts/newsletter/list',
	getCommercialReqAPI: '/api/bookings/admin/commercial_booking/list',
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
