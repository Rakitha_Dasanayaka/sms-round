export const environment = {
	production: true,
	isMockEnabled: true, // You have to switch this, when your real back-end is done
	authTokenKey: 'authce9d77b308c149d5992a80073637e4d5',
	//baseURL: 'http://34.70.53.145',
	baseURL: 'https://smsround.vision8.io',
	apiKey: '0254eada-485d-14d9-b5561-d62673d93L',

	adminLoginAPI: '/api/auth/user/login',

	// adminLoginAPI: '/api/auth/admin/loginAdmin',

	// //loyalty customer routes
	// getCustomerListAPI: '/api/transaction/customer/list',
	// createUpdateCustomerAPI: '/api/transaction/customer/update',
	// deleteCustomerAPI: '/api/transaction/customer/delete',

	// //Corporate  routes
	// getCorporateByIdAPI: '/api/transaction/corporate/getById',
	// getCorporateListAPI: '/api/transaction/corporate/list',
	 createUpdateCorporateAPI: '/api/transaction/corporate/update',
	// deleteCorporateAPI: '/api/transaction/corporate/delete',

	// //Vehicle  routes
	// getVehicleByIdAPI: '/api/transaction/vehicle/getById',
	// getVehicleListAPI: '/api/transaction/vehicle/list',
	// createUpdateVehicleAPI: '/api/transaction/vehicle/update',
	// deleteVehicleAPI: '/api/transaction/vehicle/delete',

	// // Order routes
	// createNewLoyaltyAPI: '/api/transaction/order/loyalty/create',
	// createNewGeneralAPI: '/api/transaction/order/general/create',
	// getOrdersByIdAPI: '/api/transaction/order/getById',
	// getOrderesListAPI: '/api/transaction/order/list',
	// deleteOrderAPI: '/api/transaction/order/delete',

	// //Propane Price routes
	// getPropanePriceAPI: '/api/transaction/propaneInfo',
	// updatePropanePriceAPI: '/api/transaction/propaneInfo/update',

	// //dash
	// getDashDataAPI: '/api/transaction/order/dailySales',

	getUserDetailsAPI: '/api/auth/user/myAccount',
	testAPI: '/api/app/test_version',
	getContactusAPI: '/api/contacts/contactus/list',
	getJobApplicationAPI: '/api/contacts/jobApplication/list',
	getNewsletterAPI: '/api/contacts/newsletter/list',
	getCommercialReqAPI: '/api/bookings/admin/commercial_booking/list',
};
