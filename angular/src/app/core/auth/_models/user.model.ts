import { BaseModel } from '../../_base/crud';
import { Address } from './address.model';
import { SocialNetworks } from './social-networks.model';

export class User extends BaseModel {
    id: number;
    email: string;
    password: string;
    accessToken: string;
    refreshToken: string;
    status: string;
	first_name: string;
	full_name: string;
    last_name: string;
    created_date: number;
    updated_date: number;

    clear(): void {
        this.id = undefined;
        this.password = '';
        this.email = '';
        this.accessToken = 'access-token-' ;
        this.refreshToken = 'access-token-';
        this.status = '';
        this.first_name = '';
		this.last_name = '';
		this.full_name = '';
        this.created_date = undefined;
        this.updated_date = undefined;
     }
}
