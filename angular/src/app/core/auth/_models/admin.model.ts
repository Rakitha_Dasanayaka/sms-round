import { BaseModel } from '../../_base/crud';
import { Address } from './address.model';
import { SocialNetworks } from './social-networks.model';

export class Admin extends BaseModel {
    id: number;
    role: string;
    email: string;
    password: string;
    accessToken: string;
    refreshToken: string;
    first_name: string;
    last_name: string;

    clear(): void {
        this.id = undefined;
        this.password = '';
        this.email = '';
        this.accessToken = 'access-token-' ;
        this.refreshToken = 'access-token-';
        this.first_name = '';
        this.last_name = '';
    }
}
