import { Pipe, PipeTransform } from '@angular/core';

import * as moment from 'moment';
@Pipe({ name: 'dateFormatPipe' })
export class DateFormatPipe implements PipeTransform {
  transform(v: string, arg: string): string {
    let str = moment.unix(+v).format('YYYY MMM DD - LT');
    if (arg == 'ymd') {
      str = moment.unix(+v).format('YYYY-MM-DD');
    }

    return str;
  }
}
export const MomentToDay = (format: string) => {
  return moment()
    .utc()
    .format(format);
};
export const MomentFormatDate = (date: any, format: string) => {
  return moment(date).format(format);
};
export const unixToYmd = (date: number) => {
  return moment.unix(date).format('YYYY-MM-DD');
};