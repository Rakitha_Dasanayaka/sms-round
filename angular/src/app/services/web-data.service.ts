import { Injectable } from '@angular/core';
import { HttpClient,HttpHeaders } from '@angular/common/http';
import { environment } from '../../environments/environment.prod';

import { Observable, of, forkJoin } from 'rxjs';
import { map, catchError, mergeMap, tap } from 'rxjs/operators';

@Injectable({
    providedIn: 'root'
})
export class WebDataService{
    private _baseurl: string = environment.baseURL;
    private _api_key: string =  environment.apiKey;
    private _get_job_list:  string = this._baseurl+environment.getJobApplicationAPI+"?apiKey="+this._api_key;
    private _get_newsletter_list:  string = this._baseurl+environment.getNewsletterAPI+"?apiKey="+this._api_key;
    private _get_contactus_list:  string = this._baseurl+environment.getContactusAPI+"?apiKey="+this._api_key;
    private _get_commercial_req_list:  string = this._baseurl+environment.getCommercialReqAPI+"?apiKey="+this._api_key;
    private _test_the_api:  string = this._baseurl+environment.testAPI+"?apiKey="+this._api_key;

    adsApproved : any;
    tempA : any;
    tempB : any;
    
    constructor(private http: HttpClient){ }
    
    getJobRequests(limit,skip){
        let headers = new HttpHeaders({
            'Content-Type': 'application/json' ,
            'Authorization': 'Bearer eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJfaWQiOiI1ZDQ5ODM0YzY4MjA1OTJlY2MzN2ZiYzciLCJlbWFpbCI6ImFkbWluX3N1cGVyQGdtYWlsLmNvbSIsInJvbGUiOiJTVVBFUl9BRE1JTiIsImlhdCI6MTU2NTk0NzQ1OCwiZXhwIjoxNTY4NTM5NDU4LCJhdWQiOiJodHRwOi8vamFubWFzdGVycy5jb20iLCJpc3MiOiJqYW5tYXN0ZXJzLmNvbSIsInN1YiI6Imphbm1hc3RlcnNAYWRtaW4uY29tIn0.OLXQuB0PhiMFd1aA9-WN87_lNOGyMNYg4XKCwgpfl9QSjQHL1-PUqXcqZst-Je1aFsWhIlk14v-5ntiSbYNa9ftSMGMmKE5VGVxroXIcsfAOHfzveLh4ohu18do8mbQc-LWtZD_Qw5MSvOBS9XBb9xxjx6OvQBlVUQGIofetKbY',
        });
        let url = this._get_job_list + "&limit="+limit+"&skip="+skip;
        let options = { headers: headers };
        return this.http.get(url,options);
    }

    getContactRequests(limit,skip,status){
        let headers = new HttpHeaders({
            'Content-Type': 'application/json' ,
            'Authorization': 'Bearer eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJfaWQiOiI1ZDQ5ODM0YzY4MjA1OTJlY2MzN2ZiYzciLCJlbWFpbCI6ImFkbWluX3N1cGVyQGdtYWlsLmNvbSIsInJvbGUiOiJTVVBFUl9BRE1JTiIsImlhdCI6MTU2NTk0NzQ1OCwiZXhwIjoxNTY4NTM5NDU4LCJhdWQiOiJodHRwOi8vamFubWFzdGVycy5jb20iLCJpc3MiOiJqYW5tYXN0ZXJzLmNvbSIsInN1YiI6Imphbm1hc3RlcnNAYWRtaW4uY29tIn0.OLXQuB0PhiMFd1aA9-WN87_lNOGyMNYg4XKCwgpfl9QSjQHL1-PUqXcqZst-Je1aFsWhIlk14v-5ntiSbYNa9ftSMGMmKE5VGVxroXIcsfAOHfzveLh4ohu18do8mbQc-LWtZD_Qw5MSvOBS9XBb9xxjx6OvQBlVUQGIofetKbY',
        });
        let url = this._get_contactus_list + "&limit="+limit+"&skip="+skip+"&status="+status;
        let options = { headers: headers };
        return this.http.get(url,options);
    }

    getNewsletterRequests(limit,skip){
        let headers = new HttpHeaders({
            'Content-Type': 'application/json' ,
            'Authorization': 'Bearer eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJfaWQiOiI1ZDQ5ODM0YzY4MjA1OTJlY2MzN2ZiYzciLCJlbWFpbCI6ImFkbWluX3N1cGVyQGdtYWlsLmNvbSIsInJvbGUiOiJTVVBFUl9BRE1JTiIsImlhdCI6MTU2NTk0NzQ1OCwiZXhwIjoxNTY4NTM5NDU4LCJhdWQiOiJodHRwOi8vamFubWFzdGVycy5jb20iLCJpc3MiOiJqYW5tYXN0ZXJzLmNvbSIsInN1YiI6Imphbm1hc3RlcnNAYWRtaW4uY29tIn0.OLXQuB0PhiMFd1aA9-WN87_lNOGyMNYg4XKCwgpfl9QSjQHL1-PUqXcqZst-Je1aFsWhIlk14v-5ntiSbYNa9ftSMGMmKE5VGVxroXIcsfAOHfzveLh4ohu18do8mbQc-LWtZD_Qw5MSvOBS9XBb9xxjx6OvQBlVUQGIofetKbY',
        });
        let url = this._get_newsletter_list + "&limit="+limit+"&skip="+skip;
        let options = { headers: headers };
        return this.http.get(url,options);
    }

    getCommercialRequests(limit,skip,status){
        let headers = new HttpHeaders({
            'Content-Type': 'application/json' ,
            'Authorization': 'Bearer eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJfaWQiOiI1ZDQ5ODM0YzY4MjA1OTJlY2MzN2ZiYzciLCJlbWFpbCI6ImFkbWluX3N1cGVyQGdtYWlsLmNvbSIsInJvbGUiOiJTVVBFUl9BRE1JTiIsImlhdCI6MTU2NTk0NzQ1OCwiZXhwIjoxNTY4NTM5NDU4LCJhdWQiOiJodHRwOi8vamFubWFzdGVycy5jb20iLCJpc3MiOiJqYW5tYXN0ZXJzLmNvbSIsInN1YiI6Imphbm1hc3RlcnNAYWRtaW4uY29tIn0.OLXQuB0PhiMFd1aA9-WN87_lNOGyMNYg4XKCwgpfl9QSjQHL1-PUqXcqZst-Je1aFsWhIlk14v-5ntiSbYNa9ftSMGMmKE5VGVxroXIcsfAOHfzveLh4ohu18do8mbQc-LWtZD_Qw5MSvOBS9XBb9xxjx6OvQBlVUQGIofetKbY',
        });
        let url = this._get_commercial_req_list + "&limit="+limit+"&skip="+skip+"&status="+status;
        let options = { headers: headers };
        return this.http.get(url,options);
    }

    testAPIGet(){
        let headers = new HttpHeaders({
        });
        let options = { headers: headers };
        return this.http.get(this._test_the_api,options);
    }

    testAPIPost(){
        let headers = new HttpHeaders({
            'X-Requested-With':'XMLHttpRequest',
        });
        let options = { headers: headers };
        return this.http.post(this._test_the_api,options);
    }
}