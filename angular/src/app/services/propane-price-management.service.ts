// import { Injectable } from '@angular/core';
// import { HttpClient,HttpHeaders } from '@angular/common/http';
// import { environment } from '../../environments/environment.prod';

// import { Observable, of, forkJoin } from 'rxjs';
// import { map, catchError, mergeMap, tap } from 'rxjs/operators';

// @Injectable({
//     providedIn: 'root'
// })
// export class PropanePriceManagementService{
//     private _baseurl: string = environment.baseURL;
//     private _api_key: string =  environment.apiKey;
//     private getPropanePriceURL:  string = this._baseurl+environment.getPropanePriceAPI+"?apiKey="+this._api_key;
//     private updatePropanePriceURL:  string = this._baseurl+environment.updatePropanePriceAPI+"?apiKey="+this._api_key;
//     private _test_the_api:  string = this._baseurl+environment.testAPI+"?apiKey="+this._api_key;

//     adsApproved : any;
//     tempA : any;
//     tempB : any;

//     constructor(private http: HttpClient){ }

//     getPropanePrice(){
//         let headers = new HttpHeaders({
//             'Content-Type': 'application/json' ,
//             'Authorization': 'Bearer '+localStorage.getItem('currentUserAuthToken'),
//         });
//         let url = this.getPropanePriceURL ;
//         let options = { headers: headers };
//         return this.http.get(url,options);
//     }

//     updatePropanePrice(newprice:string,points:any){
//         let headers = new HttpHeaders({
//             'Content-Type': 'application/json' ,
//             'Authorization': 'Bearer '+localStorage.getItem('currentUserAuthToken'),
//         });
//         let url = this.updatePropanePriceURL ;
//         let options = { headers: headers };
//         return this.http.post(url,{
//             "propane_price_lb":newprice,
//             "points_per_lb": points
//         },options);
//     }

//     testAPIGet(){
//         let headers = new HttpHeaders({
//         });
//         let options = { headers: headers };
//         return this.http.get(this._test_the_api,options);
//     }

//     testAPIPost(){
//         let headers = new HttpHeaders({
//             'X-Requested-With':'XMLHttpRequest',
//         });
//         let options = { headers: headers };
//         return this.http.post(this._test_the_api,options);
//     }

// }
