import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
//import { localStorage_ADMIN_Auth_token, localStorage_ADMIN_Auth_user } from 'src/app/app.constants';
import { HttpClient, HttpHeaders, HttpEvent, HttpRequest } from '@angular/common/http';
import { environment } from '../../environments/environment';

export const enum AdminAuthState {
  default = 0,
  AdminLoggedIn = 1,
  guest = 2
}

@Injectable({
  providedIn: 'root'
})

export class userProfileService {
  authToken = new BehaviorSubject<string>(null);
  authState = new BehaviorSubject<number>(AdminAuthState.default);

  private _baseurl: string = environment.baseURL;
  private _api_key: string =  environment.apiKey;
  private _request_user_profile_details:  string = this._baseurl+environment.getUserDetailsAPI+"?apiKey="+this._api_key;
  private _test_the_api:  string = this._baseurl+environment.testAPI+"?apiKey="+this._api_key;
  tempB : any;
  constructor(private http: HttpClient) {
    console.error("+++++constructor++++++");
    this.initApp();

  }
  async initApp() {
    // const v = await this.authCheck();
    // if (v) {
    //   this.authState.next(AdminAuthState.AdminLoggedIn);
    //   return true;
    // } else {
    //   this.authState.next(AdminAuthState.guest);
    // }
    // return false;
  }

  async testAPI(): Promise<any>{
    console.log("start ++++++++++++ ");
    let headers = new HttpHeaders({
    });
    console.log(headers);
    let options = { headers: headers };

    this.tempB =  this.http.get(this._test_the_api,options).toPromise();
    console.log("end ++++++++++++ ");
    // console.log(this.tempB);
    return this.tempB;
}

  async getUserAccount(): Promise<any> {
    console.error("+++++getUserAccount++++++");
    try {
      console.error("+++++++++++");
      let headers = new HttpHeaders({
        'Content-Type': 'application/json' ,
        'Authorization': 'Bearer eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJfaWQiOiI1ZDUwMDcwMjA2MzEyNTEyMDZjOTQwZDIiLCJlbWFpbCI6ImV4YW1wbGV1c2VyQGdtYWlsLmNvbSIsInJvbGUiOiJVU0VSIiwiaWF0IjoxNTY2MzU1MDY1LCJleHAiOjE1Njg5NDcwNjUsImF1ZCI6Imh0dHA6Ly9qYW5tYXN0ZXJzLmNvbSIsImlzcyI6Imphbm1hc3RlcnMuY29tIiwic3ViIjoiamFubWFzdGVyc0BhZG1pbi5jb20ifQ.bXOQnTnzdcQZCNoHOYUVGOyKs9WWpHj_pn341RRnt9bJpAer8_F93KkQj13fjqhJgvU-FdQjunFOtr5lmoXrUwjUjQH3OaWdPSM7WKFDPYmRimN6mQPepdfSRrrPMBRI8mYj9cfwNiG0YAOuQGS2bm3VF_6RUzH73ZPkjIm54mM',
        'X-Requested-With':'XMLHttpRequest',
        'Access-Control-Allow-Origin':'http://localhost:4201'
        // 'Access-Control-Allow-Origin':'*',
        // "Access-Control-Allow-Headers": "Origin, X-Requested-With, Content-Type, Accept",
        // "Access-Control-Allow-Methods" : "GET, POST, DELETE, PUT"
    });
    console.log(headers);
    let options = { headers: headers };

      console.error(headers);
      const data: any = await this.http
        .get(this._request_user_profile_details, options)
        .toPromise();
      console.log(data);
      console.error("+++++2++++++");
      return data;
    } catch (err) {
      console.error(err);
      throw err;
    }
  }
}
