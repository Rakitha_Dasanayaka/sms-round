import { Injectable } from '@angular/core';
import { HttpClient,HttpHeaders } from '@angular/common/http';
import { environment } from '../../environments/environment.prod';

import { Observable, of, forkJoin } from 'rxjs';
import { map, catchError, mergeMap, tap } from 'rxjs/operators';

@Injectable({
    providedIn: 'root'
})
export class DashboardDataService{
    private _baseurl: string = environment.baseURL;
    private _api_key: string =  environment.apiKey;
    // private getDashDataURL:  string = this._baseurl+environment.getDashDataAPI+"?apiKey="+this._api_key;
    private _test_the_api:  string = this._baseurl+environment.testAPI+"?apiKey="+this._api_key;

    adsApproved : any;
    tempA : any;
    tempB : any;

    constructor(private http: HttpClient){ }

    getDashData(){
        let headers = new HttpHeaders({
            'Content-Type': 'application/json' ,
            'Authorization': 'Bearer '+localStorage.getItem('currentUserAuthToken'),
        });
        // let url = this.getDashDataURL ;
        let options = { headers: headers };
        // console.log(url);
        // return this.http.get(url,options);
    }

    testAPIGet(){
        let headers = new HttpHeaders({
        });
        let options = { headers: headers };
        return this.http.get(this._test_the_api,options);
    }

    testAPIPost(){
        let headers = new HttpHeaders({
            'X-Requested-With':'XMLHttpRequest',
        });
        let options = { headers: headers };
        return this.http.post(this._test_the_api,options);
    }

}
