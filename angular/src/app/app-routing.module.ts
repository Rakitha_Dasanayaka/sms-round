// Angular
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
	{path: 'auth', loadChildren: 'app/views/pages/auth/auth.module#AuthModule'},
	{path: '', loadChildren: 'app/views/themes/demo4/theme.module#ThemeModule'},
	{path: 'dashboard', loadChildren: 'app/views/themes/demo4/theme.module#ThemeModule'},
	{path: '**', redirectTo: 'error/403', pathMatch: 'full'},
];

@NgModule({
	imports: [
		RouterModule.forRoot(routes)
	],
	exports: [RouterModule]
})
export class AppRoutingModule {
}
