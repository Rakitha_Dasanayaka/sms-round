// Angular
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
// Components
import { BaseComponent } from './base/base.component';
import { ErrorPageComponent } from './content/error-page/error-page.component';
// Auth
import { AuthGuard } from '../../../core/auth';


const routes: Routes = [
	{
		path: '',
		component: BaseComponent,
		canActivate: [AuthGuard],
		children: [
			{
				path: 'dashboard',
				loadChildren: () => import('app/views/pages/dashboard/dashboard.module').then(m => m.DashboardModule)
			},
			{
				path: 'material',
				loadChildren: () => import('app/views/pages/material/material.module').then(m => m.MaterialModule)
			},
			{
				path: 'customers',
				loadChildren: () => import('app/views/pages/customers/customers.module').then(m => m.CustomersModule)
			},
			{
				path: 'newsletters',
				loadChildren: () => import('app/views/pages/newsletters/newsletters.module').then(m => m.NewslettersModule)
			},
			{
				path: 'contactmessages',
				loadChildren: () => import('app/views/pages/contactmessage/contactmessage.module').then(m => m.ContactMessageModule)
			},
			{
				path: 'createbooking',
				loadChildren: () => import('app/views/pages/createbooking/createbooking.module').then(m => m.CreateBookingModule)
			},
			{
				path: 'testform',
				loadChildren: () => import('app/views/pages/testform/testform.module').then(m => m.TestFormModule)
			},
			{
				path: 'commercialbooking',
				loadChildren: () => import('app/views/pages/commercialbooking/commercialbooking.module').then(m => m.CommercialBookingModule)
			},
			{
				path: 'data',
				loadChildren: () => import('app/views/pages/pages.module').then(m => m.PagesModule)
			},
			{
				path: 'jobrequests',
				loadChildren: () => import('app/views/pages/jobrequests/jobrequests.module').then(m => m.JobRequestsModule)
			},
			{
				path: 'mail',
				loadChildren: () => import('app/views/pages/apps/mail/mail.module').then(m => m.MailModule)
			},
			{
				path: 'ecommerce',
				loadChildren: () => import('app/views/pages/apps/e-commerce/e-commerce.module').then(m => m.ECommerceModule),
			},
			{
				path: 'ngbootstrap',
				loadChildren: () => import('app/views/pages/ngbootstrap/ngbootstrap.module').then(m => m.NgbootstrapModule)
			},

			{
				path: 'user-management',
				loadChildren: () => import('app/views/pages/user-management/user-management.module').then(m => m.UserManagementModule)
			},
			{
				path: 'wizard',
				loadChildren: () => import('app/views/pages/wizard/wizard.module').then(m => m.WizardModule)
			},
			{
				path: 'builder',
				loadChildren: () => import('app/views/themes/demo6/content/builder/builder.module').then(m => m.BuilderModule)
			},
			{
				path: 'error/403',
				component: ErrorPageComponent,
				data: {
					'type': 'error-v6',
					'code': 403,
					'title': '403... Access forbidden',
					'desc': 'Looks like you don\'t have permission to access for requested page.<br> Please, contact administrator'
				}
			},
			{path: 'error/:type', component: ErrorPageComponent},
			{path: '', redirectTo: 'dashboard', pathMatch: 'full'},
			{path: '**', redirectTo: 'dashboard', pathMatch: 'full'}
		]
	},
];

@NgModule({
	imports: [RouterModule.forChild(routes)],
	exports: [RouterModule]
})
export class PagesRoutingModule {
}
