// Angular
import { Component, Input, OnInit } from '@angular/core';
// RxJS
import { Observable } from 'rxjs';
// NGRX
import { select, Store } from '@ngrx/store';
// State
import { AppState } from '../../../../../core/reducers';
import { currentUser, Logout, User } from '../../../../../core/auth';

@Component({
	selector: 'kt-user-profile3',
	templateUrl: './user-profile3.component.html',
	styleUrls: ['./user-profile3.component.scss']
})
export class UserProfile3Component implements OnInit {
	// Public properties
	user$: Observable<User>;
	userData:any;
	@Input() avatar: boolean = true;
	@Input() greeting: boolean = true;
	@Input() badge: boolean;
	@Input() icon: boolean;

	/**
	 * Component constructor
	 *
	 * @param store: Store<AppState>
	 */
	constructor(private store: Store<AppState>) {
	}

	/**
	 * @ Lifecycle sequences => https://angular.io/guide/lifecycle-hooks
	 */

	/**
	 * On init
	 */
	ngOnInit(): void {
		if (localStorage.getItem("currentUser") === null) {
			localStorage.removeItem('currentUser');
			this.store.dispatch(new Logout());
		}
		this.userData = JSON.parse(localStorage.getItem("currentUser"));
		this.user$ = this.store.pipe(select(currentUser));
	}

	/**
	 * Log out
	 */
	logout() {
		localStorage.removeItem('currentUser');
		this.store.dispatch(new Logout());
	}
}
