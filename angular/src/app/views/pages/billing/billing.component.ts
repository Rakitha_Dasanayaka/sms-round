import { Component } from '@angular/core';

export interface PeriodicElement {
	created_on: string;
	id: number;
	amount: number;
	status: string;
	method: string;
	invoice: string;
}

@Component({
	selector: 'kt-app-billing' ,
	templateUrl: './billing.component.html',
	styleUrls: ['./billing.component.scss']
})

export class BillingComponent {
	displayedColumns: string[] = ['created_on', 'id', 'amount', 'status', 'method', 'invoice'];
	dataSource;
	a: string;

}
