// Angular
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
// Core Module
import { CoreModule } from '../../../core/core.module';
import { PartialsModule } from '../../partials/partials.module';
import { ReceiveSMSComponent } from './receive-sms.component';
import { MatTabsModule, MatTableModule, MatCardModule, MatButtonModule } from '@angular/material';

@NgModule({
	imports: [
		CommonModule,
		MatTabsModule,
		MatTableModule,
		MatCardModule,
		PartialsModule,
		CoreModule,
		MatButtonModule,
		RouterModule.forChild([
			{
				path: '',
				component: ReceiveSMSComponent
			},
		]),
	],
	providers: [],
	declarations: [
		ReceiveSMSComponent,
	]
})
export class ReceiveSMSModule {
}
