import { Injectable } from '@angular/core';
import { HttpClient,HttpHeaders } from '@angular/common/http';
import { environment } from '../../../../environments/environment';

import { Observable, of, forkJoin } from 'rxjs';
import { map, catchError, mergeMap, tap } from 'rxjs/operators';

@Injectable({
    providedIn: 'root'
})
export class DashboardService{
    private _baseurl: string = environment.baseURL;
    private _api_key: string =  environment.apiKey;
    private _request_user_profile_details:  string = this._baseurl+environment.getUserDetailsAPI+"?apiKey="+this._api_key;
    private _test_the_api:  string = this._baseurl+environment.testAPI+"?apiKey="+this._api_key;

    adsApproved : any;
    tempA : any;
    tempB : any;
    
    constructor(private http: HttpClient){ }
    
    getUserProfile(){
        // console.log("start ++++++++++++ ");
        let headers = new HttpHeaders({
            'Content-Type': 'application/json' ,
            'Authorization': 'Bearer eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJfaWQiOiI1ZDUwMDcwMjA2MzEyNTEyMDZjOTQwZDIiLCJlbWFpbCI6ImV4YW1wbGV1c2VyQGdtYWlsLmNvbSIsInJvbGUiOiJVU0VSIiwiaWF0IjoxNTY2MzU1MDY1LCJleHAiOjE1Njg5NDcwNjUsImF1ZCI6Imh0dHA6Ly9qYW5tYXN0ZXJzLmNvbSIsImlzcyI6Imphbm1hc3RlcnMuY29tIiwic3ViIjoiamFubWFzdGVyc0BhZG1pbi5jb20ifQ.bXOQnTnzdcQZCNoHOYUVGOyKs9WWpHj_pn341RRnt9bJpAer8_F93KkQj13fjqhJgvU-FdQjunFOtr5lmoXrUwjUjQH3OaWdPSM7WKFDPYmRimN6mQPepdfSRrrPMBRI8mYj9cfwNiG0YAOuQGS2bm3VF_6RUzH73ZPkjIm54mM',
            'X-Requested-With':'XMLHttpRequest',
            'Access-Control-Allow-Origin':'http://localhost:4201'
            // 'Access-Control-Allow-Origin':'*',
            // "Access-Control-Allow-Headers": "Origin, X-Requested-With, Content-Type, Accept",
            // "Access-Control-Allow-Methods" : "GET, POST, DELETE, PUT"
        });
        console.log(headers);
        let options = { headers: headers };

        return this.http.get(this._request_user_profile_details,options);
        // console.log("end ++++++++++++ ");
        // console.log(this.tempB);
        // return this.tempB;
    }

    testAPIGet(){
        // console.log("start ++++++++++++ ");
        let headers = new HttpHeaders({
        });
        // console.log(headers);
        let options = { headers: headers };

        return this.http.get(this._test_the_api,options);
        // console.log("end ++++++++++++ ");
        // console.log(this.tempB);
        // return this.tempB;
    }

    testAPIPost(){
        console.log("start ++++++++++++ ");
        let headers = new HttpHeaders({
            'X-Requested-With':'XMLHttpRequest',
            'Access-Control-Allow-Origin':'http://localhost:4201',
        });
        console.log(headers);
        let options = { headers: headers };

        this.tempB =  this.http.post(this._test_the_api,options);
        console.log("end ++++++++++++ ");
        console.log(this.tempB);
        return this.tempB;
    }

    register(): Observable<any> {
        const httpHeaders = new HttpHeaders();
        httpHeaders.set('Content-Type', 'application/json');
        // httpHeaders.set('Content-Type', 'application/json');
        //  httpHeaders.set('Content-Type', 'application/json');
        return this.http.post(this._test_the_api, '', { headers: httpHeaders });
    }
}