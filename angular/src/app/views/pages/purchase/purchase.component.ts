import { Component } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

export interface Food{
	value: string;
	display: string;
}

@Component({
	selector: 'purchase' ,
	templateUrl: './purchase.component.html',
	styleUrls: ['./purchase.component.scss']
})

export class PurchaseComponent{
	title = 'materialApp';
	firstFormGroup: FormGroup;
	secondFormGroup: FormGroup;
	quantity: number;
	payment_method = 'card_pamyment' ;

	plan_list = [
			{name: 'Starter', price: '$ 18', sms: 1000, month: 1, quantity: '', color: "col1", footer: 'plan_footer', btn: 'btn_1' },
			{name: 'Lite', price: '$ 75', sms: 5000, month: 6, quantity: '', color: "col2", footer: 'plan_footer', btn: 'btn_2' },
			{name: 'Moderate', price: '$ 120', sms: 10000, month: 12, quantity: '', color: "col3", footer: 'plan_footer', btn: 'btn_3' },
			{name: 'Blaster', price: '$ 225', sms: 25000, month: 12, quantity: '', color: "col4", footer: 'plan_footer', btn: 'btn_4' },
	];


	// selected_plan={
	// 	selected_index: 1,
	// 	qty: 1
	// }


	constructor(private _formBuilder: FormBuilder) {}

	ngOnInit() {
		this.firstFormGroup = this._formBuilder.group({
			firstCtrl : ['', Validators.required]
		});
		this.secondFormGroup = this._formBuilder.group({
			secondCtrl : ['', Validators.required]
		});
	}

}
