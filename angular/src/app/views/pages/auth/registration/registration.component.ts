import { Component, OnInit } from '@angular/core';
import { FormBuilder, NgForm, FormGroup, Validators  } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { UserService } from '../../../../core/auth/_services/user.service';
import { AuthService } from '../../../../core/auth';
import { AlertService } from '../../../../core/auth/_services/alert.service';
import { first } from 'rxjs/operators';

@Component({
	selector: 'kt-login',
	templateUrl: './registration.component.html',
})

export class SignUpComponent implements OnInit {
//    firstname: string = '';
//    lastname: string = '';
//    email: string = '';
//    password: string = '';
   registerForm: FormGroup;
   model: any = {};
   private returnUrl: any;

    loading = false;

   registerUserData = [];

   constructor(
	private formBuilder: FormBuilder,
        private router: Router,
        private authService: AuthService,
        private userService: UserService,
		private alertService: AlertService,
		private route: ActivatedRoute,

   ) {
	 if (this.authService.currentUserValue) {
	 	this.router.navigate(['/']);
	 }
   }

   registerUser(form: NgForm) {
	   console.log(form.value);
   }

   ngOnInit(){
	this.registerForm = this.formBuilder.group({
		firstName: ['', Validators.required],
		lastName: ['', Validators.required],
		fullName: ['', Validators.required],
		email: ['', Validators.required],
		password: ['', [Validators.required, Validators.minLength(6)]],
		confirmPassword: ['', [Validators.required, Validators.minLength(6)]]
	 });



		// redirect back to the returnUrl before login
		// this.router.queryParams.subscribe(params => {
		// 	this.returnUrl = params['returnUrl'] || '/';
		// });
 }



   get f() {
		return this.registerForm.controls;
	}

    onSubmit() {


        // stop here if form is invalid
        if (this.registerForm.invalid) {
            return;
        }

        this.loading = true;
        this.userService.register(this.registerForm.value)
		.pipe(first())
		.subscribe(
                data => {
                    this.alertService.success('Registration successful', true);
                    this.router.navigate(['/login']);
                },
                error => {
                    this.alertService.error(error);
                    this.loading = false;
                });
    }


   /**
	 * Checking control validation
	 *
	 * @param controlName: string => Equals to formControlName
	 * @param validationType: string => Equals to valitors name
	 */
	isControlHasError(controlName: string, validationType: string): boolean {
		const control = this.registerForm.controls[controlName];
		if (!control) {
			return false;
		}

		const result = control.hasError(validationType) && (control.dirty || control.touched);
		return result;


}

}
