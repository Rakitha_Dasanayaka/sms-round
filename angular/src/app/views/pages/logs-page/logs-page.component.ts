import { Component, OnInit, ChangeDetectionStrategy, ChangeDetectorRef } from '@angular/core';
import { SelectionModel } from '@angular/cdk/collections';
import { NgbModal, NgbActiveModal} from '@ng-bootstrap/ng-bootstrap';


export interface PeriodicElement {
	position : string;
	message_id: number;
	message: string;
	sender_id: string;
	mobile_no: number;
	created_on: Date;
	status: string;
	pages: number;
	credits: number;

}

@Component({
	selector: 'ngbd-modal-content1',
	templateUrl: './logs-page-wipe-modal.component.html',
	styleUrls: ['./logs-page.component.scss']
})

export class NgbdModalContent1 {


	constructor(public activeModal: NgbActiveModal) {}
}

@Component({
	selector: 'ngbd-modal-content2',
	templateUrl: './logs-page-export-modal.component.html',
	styleUrls: ['./logs-page.component.scss']
})

export class NgbdModalContent2 {


	constructor(public activeModal: NgbActiveModal) {}
}

@Component({
	selector: 'kt-material-table',
	templateUrl: './logs-page.component.html',
	styleUrls: ['./logs-page.component.scss'],
	changeDetection: ChangeDetectionStrategy.Default,
})

export class LogsPageComponent {
	displayedColumns : string[] = ['position', 'message_id', 'message', 'sender_id', 'mobile_no', 'created_on', 'status', 'pages', 'credits'];
	dataSource;
	selection = new SelectionModel<LogsPageComponent>(true, []);


	constructor(private modalService: NgbModal) {}


	openWipeData() {
		this.modalService.open(NgbdModalContent1);
		// modalRef.componentInstance.name = 'World';
	}

	openExportData() {
		 this.modalService.open(NgbdModalContent2);
		// modalRef.componentInstance.name = 'World';
	}

	applyFilter(filterValue: string) {
		// this.dataSource.filter = filterValue.trim().toLowerCase();
	  }

}


