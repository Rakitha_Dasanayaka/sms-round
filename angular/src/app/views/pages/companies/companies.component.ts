import { Component, OnInit, ChangeDetectionStrategy, ChangeDetectorRef} from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { WebDataService } from '../../../services/web-data.service';


@Component({
	selector: 'kt-material-table',
	templateUrl: './companies.component.html',
	styleUrls: ['./companies.component.scss'],
})

export class CompaniesComponent implements OnInit{



	constructor(
		private cdr: ChangeDetectorRef,
		private http: HttpClient,
		private WebDataService: WebDataService
	){}

	ngOnInit(){

	}

}
