// Angular
import { Component, OnInit,ViewChild, ChangeDetectionStrategy,AfterViewInit } from '@angular/core';
import { MatPaginator, MatTableDataSource, MatSort } from '@angular/material';
import { SelectionModel } from '@angular/cdk/collections';
import { WebDataService } from '../../../services/web-data.service';

export interface NewsLetterData {
  _id: string;
  email: string;
  created_date: number;
}

@Component({
	selector: 'kt-material-table',
	templateUrl: './newsletters.component.html',
  styleUrls: ['newsletters.component.scss'],
  styles: [`
	.example-container {
		display: flex;
		flex-direction: column;
		max-height: 500px;
		min-width: 300px;
		position: relative;
	  }

	  .mat-table {
		overflow: auto;
		max-height: 500px;
	  }

	  .mat-header-cell.mat-sort-header-sorted {
		color: black;
	  }

	  .example-header {
		min-height: 64px;
		padding: 8px 24px 0;
	  }

	  .mat-form-field {
		font-size: 14px;
		width: 100%;
	  }

	  .mat-table {
		overflow: auto;
		max-height: 500px;
	  }
	  .mat-column-select {
		overflow: initial;
	  }
	  .example-header {
		min-height: 64px;
		display: flex;
		align-items: center;
		padding-left: 24px;
		font-size: 20px;
	  }

	  .example-table {
		overflow: auto;
		min-height: 300px;
	  }

.example-loading-shade {
	position: absolute;
	top: 0;
	left: 0;
	bottom: 56px;
	right: 0;
	background: rgba(0, 0, 0, 0.15);
	z-index: 1;
	display: flex;
	align-items: center;
	justify-content: center;
  }

  .example-rate-limit-reached {
	color: #980000;
	max-width: 360px;
	text-align: center;
  }

  /* Column Widths */
  .mat-column-number,
  .mat-column-state {
	max-width: 64px;
  }

  .mat-column-created {
	max-width: 124px;
  }
	`],
	changeDetection: ChangeDetectionStrategy.OnPush
})

export class NewslettersComponent implements OnInit, AfterViewInit {

  displayedColumns2: string[] = ['_id', 'email', 'created_date'];
  ELEMENT_DATA3: NewsLetterData[]; 
  dataSource3;
  temp:any;

  selection = new SelectionModel<NewsLetterData>(true, []);
  resultsLength = 0;
  isLoadingResults = true;
  isRateLimitReached = false;

  pagination_item_counts = [10, 25, 50];
  pagination_data = {
    total: 0,
    limit: 10,
    pageIndex: 0
  };

  constructor(
      private WebDataService: WebDataService
      ) 
      {
     
  }

  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;

    ngAfterViewInit() {}

	ngOnInit() {
    //    this.getJobRequests(this.pagination_data.limit, 0);
       this.functionNgServe();
    }

    functionNgServe(){
      this.getNewsletterRequests(this.pagination_data.limit, 0);
	}
	
	async getNewsletterRequests(limit: number, skip: number){
        this.isLoadingResults = true;
		await this.WebDataService.getNewsletterRequests(limit,skip)
			.subscribe((response)=> {
				this.isLoadingResults = false;
				this.temp = response; 
				console.log(this.temp);
				this.pagination_data.total = this.temp.paginator.count;
				this.dataSource3 = new MatTableDataSource<NewsLetterData>(this.temp.result);

				this.paginator.length = this.temp.paginator.count;
			}, error => {
				console.log(error); // The error is here
			});
    }

    pageEvent(event: any) {
      this.pagination_data.limit = event.pageSize;
      this.pagination_data.pageIndex = event.pageIndex;
      this.getNewsletterRequests(event.pageSize, event.pageIndex * event.pageSize);
    }
}
