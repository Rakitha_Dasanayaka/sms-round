import { Component, OnInit, ViewChild, AfterViewInit, ChangeDetectionStrategy, ChangeDetectorRef } from '@angular/core';
import { MatTableDataSource } from '@angular/material/table';
import { ActivatedRoute, Router } from '@angular/router';
import { MatPaginator } from '@angular/material/paginator';
import { SelectionModel } from '@angular/cdk/collections';
import { HttpClient } from '@angular/common/http';
import {merge, Observable, of as observableOf,timer} from 'rxjs';
import {take,catchError, map, startWith, switchMap} from 'rxjs/operators';
// import { CorporateService } from '../../../services/coporate-management.service';
import { LayoutUtilsService, MessageType, QueryParamsModel } from '../../../core/_base/crud';

export interface CorporateData {
//   status: string;
  _id: string;
  company_name: string;
  email: string;
  phone: string;
  message: string;
//   created_date: number;
//   update_date: number;
}

@Component({
	selector: 'kt-material-table',
	templateUrl: './manage-corporate.component.html',
	changeDetection: ChangeDetectionStrategy.Default,
	styles: [`
	.example-container {
		display: flex;
		flex-direction: column;
		max-height: 500px;
		min-width: 300px;
		position: relative;
	  }

	  .mat-table {
		overflow: auto;
		max-height: 500px;
	  }

	  .mat-header-cell.mat-sort-header-sorted {
		color: black;
	  }

	  .example-header {
		min-height: 64px;
		padding: 8px 24px 0;
	  }

	  .mat-form-field {
		font-size: 14px;
		width: 100%;
	  }

	  .mat-table {
		overflow: auto;
		max-height: 500px;
	  }
	  .mat-column-select {
		overflow: initial;
	  }
	  .example-header {
		min-height: 64px;
		display: flex;
		align-items: center;
		padding-left: 24px;
		font-size: 20px;
	  }

	  .example-table {
		overflow: auto;
		min-height: 300px;
	  }

.example-loading-shade {
	position: absolute;
	top: 0;
	left: 0;
	bottom: 56px;
	right: 0;
	background: rgba(0, 0, 0, 0.15);
	z-index: 1;
	display: flex;
	align-items: center;
	justify-content: center;
  }

  .example-rate-limit-reached {
	color: #980000;
	max-width: 360px;
	text-align: center;
  }

  /* Column Widths */
  .mat-column-number,
  .mat-column-state {
	max-width: 64px;
  }

  .mat-column-created {
	max-width: 124px;
  }
	`]
})

export class ManageCorporateComponent implements OnInit {
  // examplePaginationD;

  displayedColumns8: string[]  = ['_id','company_name','email','phone', 'note', 'actions'];

  dataSource8;

  selection = new SelectionModel<ManageCorporateComponent>(true, []);
  resultsLength = 0;
  isLoadingResults = true;
  isRateLimitReached = false;

  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;

  counter$: Observable<number>;
  count = 60;
  temp:any;
  pagination_item_counts = [10, 25, 50];
  pagination_data = {
    total: 0,
    limit: 10,
    pageIndex: 0
  };

  constructor(
	private cdr: ChangeDetectorRef,
	private http: HttpClient,
	private activatedRoute: ActivatedRoute,
	private router: Router,
	private layoutUtilsService: LayoutUtilsService
	// private CorporateService: CorporateService
	) {
  }

	ngOnInit() {
    // this.functionNgServe();
  }

//   functionNgServe(){
//     this.getCorporateList(this.pagination_data.limit, 0,"desc","no");
//     // this.dataSource8.paginator = this.paginator;
//   }

//   async getCorporateList(limit: number, skip: number, sort: string, with_vehicles: string){
//     this.isLoadingResults = true;
//     await this.CorporateService.getCorporateList(limit,skip,sort,with_vehicles)
//       .subscribe((response)=> {
//         this.isLoadingResults = false;
//         this.temp = response;
//         this.pagination_data.total = this.temp.paginator.count;
//         this.dataSource8 = new MatTableDataSource<CorporateData>(this.temp.result);
//         this.paginator.length = this.temp.paginator.count;
//       }, error => {
//         console.log(error); // The error is here
//       });
//   }

  pageEvent(event: any) {
    this.pagination_data.limit = event.pageSize;
    this.pagination_data.pageIndex = event.pageIndex;
    // this.getCorporateList(event.pageSize, event.pageIndex * event.pageSize,"desc","no");
  }

  	deleteProduct(_item) {
		const _title: string = 'Product Delete';
		const _description: string = 'Are you sure to permanently delete this product?';
		const _waitDesciption: string = 'Product is deleting...';
		const _deleteMessage = `Product has been deleted`;

		const dialogRef = this.layoutUtilsService.deleteElement(_title, _description, _waitDesciption);
		dialogRef.afterClosed().subscribe(res => {
			if (!res) {
				return;
			}

		});
	}

	editProduct(id) {
		console.log(id);
		this.router.navigate(['manage_corporate/edit', id], { relativeTo: this.activatedRoute });
	}

}
