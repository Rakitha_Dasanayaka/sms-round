import { Component, OnInit, ViewChild, AfterViewInit, ChangeDetectionStrategy, ElementRef, Input } from '@angular/core';
import { FormControl,FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatTableDataSource } from '@angular/material/table';
import { MatPaginator } from '@angular/material/paginator';
import { SelectionModel } from '@angular/cdk/collections';
import { HttpClient } from '@angular/common/http';
import {merge, Observable, of as observableOf,timer} from 'rxjs';
import {take,catchError, map, startWith, switchMap} from 'rxjs/operators';
import { WebDataService } from '../../../services/web-data.service';



@Component({
	selector: 'kt-wizard4',
	templateUrl: './testform.component.html',
	styleUrls: ['./testform.component.scss']
})
export class TestFormComponent  {
	adminRegisterForm= new FormGroup({
		firstName: new FormControl('', [Validators.required]),
		lastName: new FormControl('', [Validators.required]),
		contactno: new FormControl('', [Validators.required]),
		email:new FormControl('', [Validators.required, Validators.email]),
		password: new FormControl('', [Validators.required, Validators.minLength(6)])
	  });
	
	  submitted = false;
	  @Input('firstName') firstName: string;
	 
	  constructor() {
	  }
	
	  get f() { return this.adminRegisterForm.controls; }
	
	  onSubmit() {
		  this.submitted = true;
	
		  if (this.adminRegisterForm.invalid) {
			  return;
		  }
		 
	  }
}
