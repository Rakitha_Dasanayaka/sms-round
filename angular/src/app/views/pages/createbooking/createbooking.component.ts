import { Component, OnInit, ViewChild, AfterViewInit, ChangeDetectionStrategy, ElementRef, Input } from '@angular/core';
import { FormControl,FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
	selector: 'kt-wizard4',
	templateUrl: './createbooking.component.html',
	styleUrls: ['./createbooking.component.scss']
})

export class CreateBookingComponent implements OnInit,AfterViewInit {
  @ViewChild('wizard', {static: true}) el: ElementRef;
  registerForm= new FormGroup({
	sqft: new FormControl('', [Validators.required]),
	bedrooms: new FormControl('', [Validators.required]),
	address: new FormControl('', [Validators.required])
  });
    wizard;
	submitted = false;
	public next = false;
	transf = "false";
	currentStep = 1;
	firstStep = false;
	secondStep = false;
	// get f() { return this.adminRegisterForm.controls; }
	constructor(private formBuilder: FormBuilder) {
	}

	ngOnInit() {
	}

	ngAfterViewInit(): void {
		// Initialize form wizard
		this.wizard = new KTWizard(this.el.nativeElement, {
			startStep: 1
		});

		// Validation before going to next page
		this.wizard.on('beforeNext', (wizardObj)=> {
			console.log("beforeNext firstStep: "+this.firstStep);

			if(this.currentStep==2){
				if(this.secondStep == false){
					wizardObj.stop();
				}else{
					this.currentStep+=1;
				}
			}

			if(this.currentStep==1){
				if(this.firstStep == false){
					wizardObj.stop();
				}else{
					this.currentStep+=1;
					this.submitted = false;
				}
			}
			
			
			// if (this.registerForm.invalid) {
			// 	wizardObj.stop();
			// }
			// https://angular.io/guide/forms
			// https://angular.io/guide/form-validation

			// validate the form and use below function to stop the wizard's step
			// wizardObj.stop();
		});

		this.wizard.on('beforePrev', (wizardObj)=> {
			console.log("beforePrev: "+this.currentStep);
			this.currentStep-=1;
			console.log("beforePrev: "+this.currentStep);
		});


		// Change event
		this.wizard.on('change', function (wizard) {
			setTimeout(function () {
				KTUtil.scrollTop();
			}, 500);
		});
	}

	onCheck(){
		console.log("registerForm onCheck");
		this.submitted = true;
		if(this.currentStep == 1){
			if(this.registerForm.value.sqft == ""){
				this.firstStep = false;
				return;
			}
			if(this.registerForm.value.bedrooms == ""){
				this.firstStep = false;
				return;
			}
			this.firstStep = true;
		}
		if(this.currentStep == 2){
			if(this.registerForm.value.address == ""){
				this.secondStep = false;
				return;
			}
			this.secondStep = true;
		}
		// console.log(this.registerForm.value);
		
		// if (this.registerForm.invalid) {
		// 	this.next = false;
		// 	this.transf = "false";
		// 	// console.log("this.next+"+this.next);
        //     return;
		// }
		
		return;
	}
	
	onSubmit() {
        this.submitted = true;
		console.log("registerForm onSubmit");
        // stop here if form is invalid
        if (this.registerForm.invalid) {
            return;
        }

        alert('SUCCESS!! :-)\n\n' + JSON.stringify(this.registerForm.value))
    }
}
