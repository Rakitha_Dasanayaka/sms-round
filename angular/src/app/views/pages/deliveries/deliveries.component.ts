import { Component, OnInit, ViewChild, AfterViewInit, ChangeDetectionStrategy, ChangeDetectorRef } from '@angular/core';
import { MatTableDataSource } from '@angular/material/table';
import { MatPaginator } from '@angular/material/paginator';
import { SelectionModel } from '@angular/cdk/collections';
import { HttpClient } from '@angular/common/http';
import {merge, Observable, of as observableOf,timer} from 'rxjs';
import {take,catchError, map, startWith, switchMap} from 'rxjs/operators';
import { WebDataService } from '../../../services/web-data.service';

export interface ContactMessagesData {
  status: string;
  _id: string;
  name: string;
  email: string;
  phone: string;
  message: string;
  created_date: number;
  update_date: number;
}

@Component({
	selector: 'kt-material-table',
	templateUrl: './deliveries.component.html',
	changeDetection: ChangeDetectionStrategy.Default,
	styles: [`
	.example-container {
		display: flex;
		flex-direction: column;
		max-height: 500px;
		min-width: 300px;
		position: relative;
	  }

	  .mat-table {
		overflow: auto;
		max-height: 500px;
	  }

	  .mat-header-cell.mat-sort-header-sorted {
		color: black;
	  }

	  .example-header {
		min-height: 64px;
		padding: 8px 24px 0;
	  }

	  .mat-form-field {
		font-size: 14px;
		width: 100%;
	  }

	  .mat-table {
		overflow: auto;
		max-height: 500px;
	  }
	  .mat-column-select {
		overflow: initial;
	  }
	  .example-header {
		min-height: 64px;
		display: flex;
		align-items: center;
		padding-left: 24px;
		font-size: 20px;
	  }

	  .example-table {
		overflow: auto;
		min-height: 300px;
	  }

.example-loading-shade {
	position: absolute;
	top: 0;
	left: 0;
	bottom: 56px;
	right: 0;
	background: rgba(0, 0, 0, 0.15);
	z-index: 1;
	display: flex;
	align-items: center;
	justify-content: center;
  }

  .example-rate-limit-reached {
	color: #980000;
	max-width: 360px;
	text-align: center;
  }

  /* Column Widths */
  .mat-column-number,
  .mat-column-state {
	max-width: 64px;
  }

  .mat-column-created {
	max-width: 124px;
  }
	`]
})
export class DeliveriesComponent implements OnInit {
  // examplePaginationD;

  displayedColumns8: string[]  = ['_id','name','email','phone', 'message', 'created_date'];

  dataSource8;

  selection = new SelectionModel<DeliveriesComponent>(true, []);
  resultsLength = 0;
  isLoadingResults = true;
  isRateLimitReached = false;

  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;

  counter$: Observable<number>;
  count = 60;
  temp:any;
  pagination_item_counts = [10, 25, 50];
  pagination_data = {
    total: 0,
    limit: 10,
    pageIndex: 0
  };

  constructor(private cdr: ChangeDetectorRef,private http: HttpClient,private WebDataService: WebDataService) {
  }

	ngOnInit() {
    this.functionNgServe();
  }

  functionNgServe(){
    this.getContactRequests(this.pagination_data.limit, 0,"any");
    // this.dataSource8.paginator = this.paginator;
  }
  
  async getContactRequests(limit: number, skip: number,status:string){
    this.isLoadingResults = true;
    await this.WebDataService.getContactRequests(limit,skip,status)
      .subscribe((response)=> {
        this.isLoadingResults = false;
        this.temp = response; 
        this.pagination_data.total = this.temp.paginator.count;
        this.dataSource8 = new MatTableDataSource<ContactMessagesData>(this.temp.result);
        this.paginator.length = this.temp.paginator.count;
      }, error => {
        console.log(error); // The error is here
      });
  }

  pageEvent(event: any) {
    this.pagination_data.limit = event.pageSize;
    this.pagination_data.pageIndex = event.pageIndex;
    this.getContactRequests(event.pageSize, event.pageIndex * event.pageSize,"any");
  }

}
