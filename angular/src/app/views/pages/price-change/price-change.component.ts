// import { ChangeDetectorRef,Component, OnInit, ViewChild, AfterViewInit, ChangeDetectionStrategy, ElementRef, Input } from '@angular/core';
// import { FormControl,FormBuilder, FormGroup, Validators } from '@angular/forms';
// import { PropanePriceManagementService } from '../../../services/propane-price-management.service';
// import { NgbAlertConfig } from '@ng-bootstrap/ng-bootstrap';
// import { ToastrService } from 'ngx-toastr';

// @Component({
// 	selector: '',
// 	templateUrl: './price-change.component.html',
// 	styleUrls: ['./price-change.component.scss']
// })

// export class PriceChangeComponent implements OnInit,AfterViewInit {
// //   @ViewChild('wizard', {static: true}) el: ElementRef;
// 	priceUpdateForm= new FormGroup({
// 		new_price: new FormControl('', [Validators.required])
// 	});
// 	staticAlertClosed = false;
// 	temp:any;
// 	currentPropanePrice:string="Loading ...";
// 	currentPoints:any=0;
// 	submitted = false;
// 	isConfirmed = false;

// 	constructor(
// 		private formBuilder: FormBuilder,
// 		private PropanePriceManagementService: PropanePriceManagementService,
// 		alertConfig: NgbAlertConfig,
// 		private cdr: ChangeDetectorRef,
// 		private toastr: ToastrService
// 		)
// 		{
// 	}

// 	ngOnInit() {
// 	}

// 	ngAfterViewInit(): void {
// 		this.functionNgServe();
// 		console.log(this.currentPropanePrice);
// 	}

// 	onSubmit() {
// 		this.submitted = true;
// 		this.isConfirmed = false;
//         // stop here if form is invalid
//         if (this.priceUpdateForm.invalid) {
//             return;
// 		}
// 		this.updatePropanePrice(this.priceUpdateForm.value.new_price,this.currentPoints);

// 	}

// 	dispenserUpdated(){
// 		this.isConfirmed = true;
// 	}

// 	functionNgServe(){
// 		this.getPropanePrice();
// 	  }

// 	async getPropanePrice(){
// 		await this.PropanePriceManagementService.getPropanePrice()
// 		  .subscribe((response)=> {
// 			this.temp = response;
// 			this.currentPropanePrice = "$"+this.temp.propane_price_lb;
// 			this.currentPoints = this.temp.points_per_lb;
// 			this.cdr.markForCheck();
// 		  }, error => {
// 			this.toastr.warning("Something went wrong.","Error!");
// 		  });
// 	  }


// 	  async updatePropanePrice(newprice:string,points:any){
// 		await this.PropanePriceManagementService.updatePropanePrice(newprice,points)
// 		  .subscribe((response)=> {
// 			this.temp = response;
// 			this.currentPropanePrice = "$"+this.temp.propane_price_lb;
// 			this.cdr.markForCheck();
// 			this.toastr.success("Propane price successfully updated","Success!");
// 			this.priceUpdateForm.reset();
// 		  }, error => {
// 			this.toastr.warning("Something went wrong.","Error!");
// 		  });
// 	  }
// }
