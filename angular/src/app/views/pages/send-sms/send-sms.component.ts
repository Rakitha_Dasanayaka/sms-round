import { Component, Input, OnInit } from '@angular/core';

import { NgbModal, NgbActiveModal} from '@ng-bootstrap/ng-bootstrap';
import { FormBuilder, FormGroup, Validators} from '@angular/forms';
import { Router } from '@angular/router';
import { AuthService } from '../../../core/auth';
import { UserService } from '../../../core/auth/_services/user.service';
import { AlertService } from '../../../core/auth/_services/alert.service';

@Component({
	selector: 'ngbd-modal-content',
	templateUrl: './send-sms-modal.component.html',
	styleUrls: ['./send-sms.component.scss']
})

export class NgbdModalContent1 implements OnInit{
	@Input() name;
	requestIdForm: FormGroup;



	constructor(
		public activeModal: NgbActiveModal,
		private formBuilder: FormBuilder,
        private router: Router,
        private authService: AuthService,
        private userService: UserService,
        private alertService: AlertService) {}

	ngOnInit(){
		this.requestIdForm = this.formBuilder.group({
			id: ['', [Validators.required, Validators.minLength(3)]],
			company: ['', Validators.required],
			businessNature: ['', Validators.required],
			name: ['', Validators.required],
			designation: ['', [Validators.required, Validators.minLength(6)]],
			confirmPassword: ['', [Validators.required, Validators.minLength(6)]]


		 });
		}

		sendRequest(){

		}

	/**
	 * Checking control validation
	 *
	 * @param controlName: string => Equals to formControlName
	 * @param validationType: string => Equals to valitors name
	 */
	isControlHasError(controlName: string, validationType: string): boolean {
		const control = this.requestIdForm.controls[controlName];
		if (!control) {
			return false;
		}

		const result = control.hasError(validationType) && (control.dirty || control.touched);
		return result;


}
}

@Component({
	selector: 'ngbd-modal-component',
	templateUrl: './send-sms.component.html',
	styleUrls: ['./send-sms.component.scss']

})
export class SendSMSComponent {

	constructor(private modalService: NgbModal) {}

	openDialog() {
		const modalRef = this.modalService.open(NgbdModalContent1);
		// modalRef.componentInstance.name = 'World';
	}

    // private getDismissReason(reason: any): string {
    //     if (reason === ModalDismissReasons.ESC) {
    //         return 'by pressing ESC';
    //     } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
    //         return 'by clicking on a backdrop';
    //     } else {
    //         return  `with: ${reason}`;
    //     }
    // }

}


