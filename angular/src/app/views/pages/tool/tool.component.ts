import { Component } from '@angular/core';
import { NgbActiveModal, NgbModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'ngbd-modal-content',
  templateUrl : './request-sender-id-modal.component.html',
  styleUrls : ['./tool.component.scss']

})
export class NgbdModalContent3 {


  constructor(public activeModal: NgbActiveModal) {}
}

@Component({
  selector: 'ngbd-modal-component',
  templateUrl: './tool.component.html',
  styleUrls : ['./tool.component.scss']
})
export class ToolComponent {
  constructor(private modalService: NgbModal) {}

  openModal() {
	this.modalService.open(NgbdModalContent3);

  }
}
