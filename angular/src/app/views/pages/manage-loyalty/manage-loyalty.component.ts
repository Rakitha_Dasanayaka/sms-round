import { Component, OnInit, ViewChild, AfterViewInit, ChangeDetectionStrategy, ChangeDetectorRef } from '@angular/core';
import { MatTableDataSource } from '@angular/material/table';
import { ActivatedRoute, Router } from '@angular/router';
import { MatPaginator } from '@angular/material/paginator';
import { SelectionModel } from '@angular/cdk/collections';
import { HttpClient } from '@angular/common/http';
import {merge, Observable, of as observableOf,timer} from 'rxjs';
// import { CustomerService } from '../../../services/customer-management.service';
import { LayoutUtilsService, MessageType, QueryParamsModel } from '../../../core/_base/crud';
import {
	ProductModel,
	ProductsDataSource,
	ProductsPageRequested,
	OneProductDeleted,
	ManyProductsDeleted,
	ProductsStatusUpdated,
	selectProductsPageLastQuery
} from '../../../core/e-commerce';

export interface CustomerData {
  cus_type: string;
  _id: string;
  name: string;
  email: string;
  phone: string;
  birthday: string;
  qr_code: string;
  note: string;
  created_date: number;
  update_date: number;
}

@Component({
	selector: 'kt-material-table',
	templateUrl: './manage-loyalty.component.html',
	changeDetection: ChangeDetectionStrategy.Default,
	styles: [`
	.example-container {
		display: flex;
		flex-direction: column;
		max-height: 500px;
		min-width: 300px;
		position: relative;
	  }

	  .mat-table {
		overflow: auto;
		max-height: 500px;
	  }

	  .mat-header-cell.mat-sort-header-sorted {
		color: black;
	  }

	  .example-header {
		min-height: 64px;
		padding: 8px 24px 0;
	  }

	  .mat-form-field {
		font-size: 14px;
		width: 100%;
	  }

	  .mat-table {
		overflow: auto;
		max-height: 500px;
	  }
	  .mat-column-select {
		overflow: initial;
	  }
	  .example-header {
		min-height: 64px;
		display: flex;
		align-items: center;
		padding-left: 24px;
		font-size: 20px;
	  }

	  .example-table {
		overflow: auto;
		min-height: 300px;
	  }

.example-loading-shade {
	position: absolute;
	top: 0;
	left: 0;
	bottom: 56px;
	right: 0;
	background: rgba(0, 0, 0, 0.15);
	z-index: 1;
	display: flex;
	align-items: center;
	justify-content: center;
  }

  .example-rate-limit-reached {
	color: #980000;
	max-width: 360px;
	text-align: center;
  }

  /* Column Widths */
  .mat-column-number,
  .mat-column-state {
	max-width: 64px;
  }

  .mat-column-created {
	max-width: 124px;
  }
	`]
})
export class ManageLoyaltyComponent implements OnInit {
  // examplePaginationD;

  displayedColumns8: string[]  = ['name','email','phone', 'cus_type', 'created_date', 'updated_date', 'actions'];

  dataSource8;

  selection = new SelectionModel<ManageLoyaltyComponent>(true, []);
  resultsLength = 0;
  isLoadingResults = true;
  isRateLimitReached = false;

  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;

  counter$: Observable<number>;
  count = 60;
  temp:any;
  pagination_item_counts = [10, 25, 50];
  pagination_data = {
    total: 0,
    limit: 10,
    pageIndex: 0
  };

  constructor(
    private cdr: ChangeDetectorRef,
    private http: HttpClient,
    private activatedRoute: ActivatedRoute,
    private router: Router,
    private layoutUtilsService: LayoutUtilsService,
	// private CustomerService: CustomerService
	)
    {
  }

	ngOnInit() {
    this.functionNgServe();
  }

  functionNgServe(){
    // this.getCustomerList(this.pagination_data.limit, 0,"","desc","no");
    // this.dataSource8.paginator = this.paginator;
  }

//   async getCustomerList(limit: number, skip: number, keyword:string, sort:string, with_vehicles: string){
//     this.isLoadingResults = true;
//     await this.CustomerService.getCustomerList(limit,skip,keyword,sort,with_vehicles)
//       .subscribe((response)=> {
//         this.isLoadingResults = false;
//         this.temp = response;
//         this.pagination_data.total = this.temp.paginator.count;
//         this.dataSource8 = new MatTableDataSource<CustomerData>(this.temp.result);
//         this.paginator.length = this.temp.paginator.count;
//       }, error => {
//         console.log(error); // The error is here
//       });
//   }

  pageEvent(event: any) {
    this.pagination_data.limit = event.pageSize;
    this.pagination_data.pageIndex = event.pageIndex;
    // this.getCustomerList(event.pageSize, event.pageIndex * event.pageSize,"","desc","no");
  }

  editProduct(id) {
		this.router.navigate(['edit', id], { relativeTo: this.activatedRoute });
  }

  deleteProduct(_item: ProductModel) {
		const _title: string = 'Product Delete';
		const _description: string = 'Are you sure to permanently delete this product?';
		const _waitDesciption: string = 'Product is deleting...';
		const _deleteMessage = `Product has been deleted`;


		const dialogRef = this.layoutUtilsService.deleteElement(_title, _description, _waitDesciption);
		dialogRef.afterClosed().subscribe(res => {
			if (!res) {
				return;
			}

			this.layoutUtilsService.showActionNotification(_deleteMessage, MessageType.Delete);
		});
	}

}
