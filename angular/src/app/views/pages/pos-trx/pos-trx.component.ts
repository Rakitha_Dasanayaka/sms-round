import { ChangeDetectorRef,Component, OnInit, ViewChild, AfterViewInit, ChangeDetectionStrategy, ElementRef, Input, ViewEncapsulation } from '@angular/core';
import { FormControl,FormBuilder, FormGroup, Validators } from '@angular/forms';
import { NgbModal, NgbActiveModal, ModalDismissReasons, NgbModalOptions } from '@ng-bootstrap/ng-bootstrap';
// import { PropanePriceManagementService } from '../../../services/propane-price-management.service';
import { ToastrService } from 'ngx-toastr';

const largeModal = {
	beforeCodeTitle: 'Large modal',
	htmlCode: `
<div class="kt-section">
  <div class="kt-section__content">
    <ng-template #content6 let-c="close" let-d="dismiss">
      <div class="modal-header">
        <h4 class="modal-title">Modal title</h4>
        <button type="button" class="close" aria-label="Close" (click)="d('Cross click')">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <p>
		  Cras mattis consectetur purus sit amet fermentum. Cras justo odio, dapibus ac facilisis in, egestas eget quam.
		  Morbi leo risus, porta ac consectetur ac, vestibulum at eros. Cras mattis consectetur purus sit amet fermentum.
          Cras justo odio, dapibus ac facilisis in, egestas eget quam. Morbi leo risus, porta ac consectetur ac, vestibulum at eros.
        </p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" (click)="c('Close click')">Close</button>
        <button type="button" class="btn btn-primary">Save changes</button>
      </div>
    </ng-template>
    <button class="btn btn-success" (click)="openLarge(content6)">Launch demo modal</button>
  </div>
</div>
`,
	tsCode: `
import {Component, ViewEncapsulation} from '@angular/core';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';\n
@Component({
    selector: 'ngbd-modal-largemodal',
    templateUrl: './modal-largemodal.html',
})
export class NgbdModalLargeModal {
    constructor(private modalService: NgbModal) {}\n
    openLarge(content) {
        this.modalService.open(content, {
            size: 'lg'
        });
    }
}
`,
	isCodeVisible: false,
	isExampleExpanded: true
};

export interface OrderItems{
    id:number;
	item_name:string;
	item_qty:string;
	item_unit_price:string;
	item_price_total:string;
	item_hst:string;
}

@Component({
	selector: 'kt-ngbd-modal-content',
	template: `
	  <div class="modal-header">
		<h4 class="modal-title">Hi there!</h4>
		<button type="button" class="close" aria-label="Close" (click)="activeModal.dismiss('Cross click')">
		  <span aria-hidden="true">&times;</span>
		</button>
	  </div>
	  <div class="modal-body">
		<p>Hello, {{name}}!</p>
	  </div>
	  <div class="modal-footer">
		<button type="button" class="btn btn-info" (click)="activeModal.close('Close click')">Close</button>
	  </div>
	`
  })
export class NgbdModalContentComponentX {
	@Input() name;
	constructor(public activeModal: NgbActiveModal) {}
  }

@Component({
	selector: 'kt-wizard3',
	templateUrl: './pos-trx.component.html',
	styleUrls: ['./pos-trx.component.scss'],
	encapsulation: ViewEncapsulation.None,
})

export class PosTrxComponent implements OnInit,AfterViewInit {
	@ViewChild('wizard', {static: true}) el: ElementRef;

	exampleModalWithDefaultOptions;
	exampleComponentsAsContent;
	exampleModalWithCustomClass;
	exampleScrollableFixedContent;
	exampleScrollingLongContent;
	exampleLargeModal;
	exampleSmallModal;
	exampleVerticallyCentered;

	autoPropaneSection: boolean = true;
	exchangePropaneSection: boolean = false;
	refillPropaneSection: boolean = false;
	loyaltyCustomerSelected: boolean = false;
	corporateCustomerSelected: boolean = false;
	creditCardSelected: boolean = false;

	closeResult: string;
	closeResult2: string;
	currentPropanePrice:any;
	temp:any;
	tempNoOfLiters ="0L";

	public myOrderItems:OrderItems[] = [
		{id: 1, item_name: 'Auto Propane',item_qty: '80.056 L',item_unit_price: '0.499',item_price_total: '39.95',item_hst: ""},
		{id: 2, item_name: 'Propane Tank Refill 20-38lb',item_qty: '56',item_unit_price: '1.499',item_price_total: '180.95',item_hst: ""},
	];
	orderBillSubTotal:any=0.00;
	orderBillHST:any=0.00;
	orderBilTotal:any=0.00;
	orderBillCashRecieved = 0.00;
	orderBillAmountOnTerminal = 0.00;
	orderBillPaymentDue = 0.00;

	isAddToOrderActive = false;

	model: any = {
		address1: 'Address Line 1',
		address2: 'Address Line 2',
		postcode: '3000',
		city: 'Melbourne',
		state: 'VIC',
		country: 'AU',
		package: 'Complete Workstation (Monitor, Computer, Keyboard & Mouse)',
		weight: '25',
		width: '110',
		height: '90',
		length: '150',
		delivery: 'overnight',
		packaging: 'regular',
		preferreddelivery: 'morning',
		locaddress1: 'Address Line 1',
		locaddress2: 'Address Line 2',
		locpostcode: '3072',
		loccity: 'Preston',
		locstate: 'VIC',
		loccountry: 'AU',
	};
	submitted = false;

	constructor(
		private modalService: NgbModal,
		// private PropanePriceManagementService: PropanePriceManagementService,
		private cdr: ChangeDetectorRef,
		private toastr: ToastrService
		) {
			// this.getPropanePrice();
	}

	ngOnInit() {
		this.exampleLargeModal = largeModal;
		this.calculateOrderSummary();
	}

	confirmOnClick(){
		this.isAddToOrderActive = true;
	}

	onQtyUpdated(id:any,itemQTY:any){
		console.log(id +"==="+ itemQTY);
		var index = this.myOrderItems.findIndex(p => p.id == id);
		console.log(this.myOrderItems[index]);
		this.myOrderItems[index].item_qty = itemQTY;
		this.myOrderItems[index].item_price_total = ((parseFloat(this.myOrderItems[index].item_unit_price) * parseFloat(this.myOrderItems[index].item_qty))*113/100).toFixed(2);
		this.myOrderItems[index].item_hst = (parseFloat(this.myOrderItems[index].item_price_total)*13/100).toFixed(2);

		if(this.myOrderItems[index].item_name!="Auto Propane"){
			this.myOrderItems[index].item_price_total = ((parseFloat(this.myOrderItems[index].item_unit_price) * parseFloat(this.myOrderItems[index].item_qty))*113/100).toFixed(2);
			this.myOrderItems[index].item_hst = (parseFloat(this.myOrderItems[index].item_price_total)*13/100).toFixed(2);
		}else{
			this.myOrderItems[index].item_price_total = (parseFloat(this.myOrderItems[index].item_unit_price) * parseFloat(this.myOrderItems[index].item_qty)).toFixed(2);
			this.myOrderItems[index].item_hst ='';
		}
	}

	onCashAmountEntered(cashAmount:any){
		this.orderBillCashRecieved = 0.00;
		this.orderBillAmountOnTerminal = 0.00;
		this.orderBillPaymentDue = 0.00;
		this.orderBillCashRecieved=cashAmount;
		let temp1 = this.orderBilTotal - this.orderBillCashRecieved;
		if(temp1<0){
			this.orderBillAmountOnTerminal = 0;
		}else{
			this.orderBillAmountOnTerminal = +temp1.toFixed(2);
		}
		this.orderBillPaymentDue = +((Math.abs(this.orderBilTotal - this.orderBillCashRecieved)).toFixed(2));
		this.cdr.markForCheck();
	}

	performLiterCount(dollarAmount:any){
		let noOfLitters = dollarAmount / this.currentPropanePrice;
		this.tempNoOfLiters = noOfLitters.toFixed(2) + "L";
		this.cdr.markForCheck();
	}

	calculateOrderSummary(){
		this.orderBillSubTotal=0.00;
		this.orderBillHST=0.00;
		this.orderBilTotal=0.00;
		this.myOrderItems.forEach(element => {
			this.orderBillSubTotal+=parseFloat(element.item_price_total);
		});
		this.orderBillSubTotal=this.orderBillSubTotal.toFixed(2);
		this.orderBillHST = this.orderBillSubTotal * 13/100;
		this.orderBillHST = this.orderBillHST.toFixed(2);
		this.orderBilTotal = this.orderBillSubTotal;
		this.cdr.markForCheck();
	}

	addToOrderArray(item_name:string,item_qty:string,item_unit_price:string,item_price_total:string,){
		console.log(item_name);console.log(item_qty);console.log(item_unit_price);console.log(item_price_total);
		if(item_name!="Auto Propane"){
			var temptotal = (parseFloat(item_unit_price)*113/100).toFixed(2);
			var tempHST = (parseFloat(item_unit_price)*13/100).toFixed(2);
			var tempRecord = {id: this.myOrderItems.length+1, item_name: item_name,item_qty: item_qty,item_unit_price: item_unit_price,item_price_total: temptotal,item_hst:tempHST};
		}else{
			var tempRecord = {id: this.myOrderItems.length+1, item_name: item_name,item_qty: item_qty,item_unit_price: item_unit_price,item_price_total: item_price_total,item_hst:''};
		}

		this.myOrderItems.push(tempRecord);
		this.calculateOrderSummary();
		this.cdr.markForCheck();
	}

	removeFromOrderArray(item_id:number){
		var index = this.myOrderItems.findIndex(p => p.id == item_id);
		this.myOrderItems.splice(index, 1);
		this.calculateOrderSummary();
		this.cdr.markForCheck();
	}

	addTankRefilItemToOrder(item_name:string,item_qty:any,item_unit_price:string,item_price_total:any){
		this.isAddToOrderActive = false;
		if(item_name=="Auto Propane"){
			this.tempNoOfLiters ="0L";
		}
		if(item_qty!="0L"){
			this.addToOrderArray(item_name,item_qty,item_unit_price,item_price_total);
		}else{
			this.toastr.warning("No of liters shouldn't be 0.","Error!");
		}

	}

	onCashSelected(){
		this.creditCardSelected = false;
	}

	onCreditSelected(){
		this.creditCardSelected = true;
	}

	onRadioClickGeneral(){
		this.loyaltyCustomerSelected = false;
		this.corporateCustomerSelected = false;
	}

	onRadioClickLoyalty(){
		this.loyaltyCustomerSelected = true;
		this.corporateCustomerSelected = false;
	}

	onRadioClickCorporate(){
		this.corporateCustomerSelected = true;
		this.loyaltyCustomerSelected = false;
	}

	onRadioClickEnableAutoPropane(){
		this.autoPropaneSection = true;
		this.exchangePropaneSection = false;
		this.refillPropaneSection = false;
	}

	onRadioClickEnableRefillPropane(){
		this.autoPropaneSection = false;
		this.exchangePropaneSection = false;
		this.refillPropaneSection = true;
	}

	onRadioClickEnableExchangePropane(){
		this.autoPropaneSection = false;
		this.exchangePropaneSection = true;
		this.refillPropaneSection = false;
	}

	open(content) {
		this.modalService.open(content).result.then((result) => {
			this.closeResult = `Closed with: ${result}`;
		}, (reason) => {
			this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
		});
	}

	open2() {
		const modalRef = this.modalService.open(NgbdModalContentComponentX);
		modalRef.componentInstance.name = 'World';
	}

	open3(content) {
		this.modalService.open(content, { windowClass: 'dark-modal' });
	}

	private getDismissReason(reason: any): string {
		if (reason === ModalDismissReasons.ESC) {
			return 'by pressing ESC';
		} else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
			return 'by clicking on a backdrop';
		} else {
			return `with: ${reason}`;
		}
	}

	openLarge(content) {
		this.modalService.open(content, {
			size: 'lg'
		});
	}

	openSmall(content) {
		this.modalService.open(content, {
			size: 'sm'
		});
	}

	openCentred(content) {
		this.modalService.open(content,
			// { centered: true }
		);
	}

	// async getPropanePrice(){
	// 	await this.PropanePriceManagementService.getPropanePrice()
	// 	  .subscribe((response)=> {
	// 		this.temp = response;
	// 		this.currentPropanePrice = this.temp.propane_price_lb;
	// 		this.cdr.markForCheck();
	// 	  }, error => {
	// 		this.toastr.warning("Something went wrong.","Error!");
	// 	  });
	//   }

	ngAfterViewInit(): void {
		// Initialize form wizard
		const wizard = new KTWizard(this.el.nativeElement, {
			startStep: 1
		});

		// Validation before going to next page
		wizard.on('beforeNext', function (wizardObj) {
			// https://angular.io/guide/forms
			// https://angular.io/guide/form-validation

			// validate the form and use below function to stop the wizard's step
			// wizardObj.stop();
		});

		// Change event
		wizard.on('change', function (wizard) {
			setTimeout(function () {
				KTUtil.scrollTop();
			}, 500);
		});
	}

	onSubmit() {
		this.submitted = true;
	}
}
